package com.smarterd.util;

import java.sql.*;
import java.util.*;
import org.apache.wink.json4j.OrderedJSONObject;
import org.apache.wink.json4j.JSONObject;
import org.apache.wink.json4j.JSONArray;

public class LoadAttributeMetadata {

	private Connection conn = null;
	private String db = null;
	private DatabaseMetaData metadata = null;

	public LoadAttributeMetadata(String host, String db) throws Exception {
		this.db = db;
		String userName = "root", password = "smarterD2018!";
		
    	Properties connectionProps = new Properties();
    	connectionProps.put("user", userName);
    	connectionProps.put("password", password);
    	
    	conn = DriverManager.getConnection("jdbc:mysql://"+host+":3306/"+db+"?useOldAliasMetadataBehavior=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&allowPublicKeyRetrieval=true", connectionProps);
		metadata = conn.getMetaData();
		conn.setAutoCommit(false);
	}

    /**
     *
     * @return Arraylist with the table's name
     * @throws SQLException
     */
    private ArrayList getTablesMetadata() throws SQLException {
        String table[] = { "TABLE" };
        ResultSet rs = null;
        ArrayList tables = null;
        
        // receive the Type of the object in a String array.
        rs = metadata.getTables(null, null, "%", table);
        tables = new ArrayList();
        while (rs.next()) {
            tables.add(rs.getString("TABLE_NAME"));
        }
        
        return tables;
    }
    
    /**
     * Prints in the console the columns metadata, based in the Arraylist of tables passed as parameter.
     * @param tables
     * @throws SQLException
     */
    private void getColumnsMetadata(ArrayList tables) throws SQLException {
        ResultSet rs = null;
        // Print the columns properties of the actual table
        for (int i=0; i<tables.size(); i++) {
	        String actualTable = (String)tables.get(i);
            rs = metadata.getColumns(null, null, actualTable, null);
            System.out.println(actualTable.toUpperCase());
            if(actualTable.equalsIgnoreCase("enterprise_asset")) {
	            while (rs.next()) {
                	System.out.println(rs.getString("COLUMN_NAME") + " " + rs.getString("TYPE_NAME") + " " + rs.getString("COLUMN_SIZE"));
            	}
            	System.out.println("\n");
            	break;
            }
        }
    }
    
    private void updateLifecycleFormat() throws Exception {
    	String sql = "select id, asset_lifecycle_list from enterprise_asset";
    	String updateSQL = "update enterprise_asset set asset_lifecycle_list = ? where id = ?";
    	PreparedStatement pst = conn.prepareStatement(sql);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
        	JSONArray arr = null;
        	JSONObject lifecycleObj = null;
        	String id = rs.getString("id");
            String lifecycleList = rs.getString("asset_lifecycle_list");
            System.out.println(lifecycleList);
            if(lifecycleList != null && lifecycleList.length() > 0) {
           		try {
           			arr = new JSONArray(lifecycleList);
           			System.out.println("Processing JSON Array...");
           			if(arr.length() == 1) {
           				boolean current = true;
           				lifecycleObj = (JSONObject)arr.getIndex(0);
           				String s=null, startdate="", enddate="";
           				long lifecycleId = 0;
           				if(lifecycleObj.get("id") instanceof Long) {
           					lifecycleId = ((Long)lifecycleObj.get("id")).longValue();
           				} else if(lifecycleObj.get("id") instanceof Integer) {
           					lifecycleId = (long)((Integer)lifecycleObj.get("id")).intValue();
           				} else {
           					try {
           						lifecycleId = Long.parseLong((String)lifecycleObj.get("id"));
           					} catch(Exception e) {
           						lifecycleId = System.currentTimeMillis();
           					}
           				}
           				if(lifecycleObj.has("current")) {
           					if(lifecycleObj.get("current") instanceof Boolean) {
           						s = ((Boolean)lifecycleObj.get("current")).toString();
           					} else {
           						s = (String)lifecycleObj.get("current");
           					}
           					if(s.equals(Double.toString(lifecycleId))) {
           						current = true;
           					} else if(s.equals("true")) {
           						current = true;
           					} else if(s.equals("false")) {
           						current = false;
           					} else if(s.equals("Y")) {
           						current = true;
           					} else if(s.equals("N")) {
           						current = false;
           					}
           				} else {
           					current = true;
           				}
           				if(lifecycleObj.has("startdate")) {
           					startdate = (String)lifecycleObj.get("startdate");
           					if(startdate.indexOf("\\") != -1) {
           						startdate = startdate.replaceAll("\\","");
           					}
           				}
           				if(lifecycleObj.has("enddate")) {
           					enddate = (String)lifecycleObj.get("enddate");
           					if(enddate.indexOf("\\") != -1) {
           						enddate = enddate.replaceAll("\\","");
           					}
           				}
           				lifecycleObj.put("id", lifecycleId);
           				lifecycleObj.put("current", current);
           				lifecycleObj.put("startdate", startdate);
           				lifecycleObj.put("enddate", enddate);
           				
           				arr.clear();
           				arr.add(lifecycleObj);
           			}
           			System.out.println("Processed JSON Array");
           		} catch(Exception ex) {
           			System.out.println(ex.getMessage());
           			JSONObject lifecycleListObj = new JSONObject(lifecycleList);
           			System.out.println("Processing JSON Object...");
           			if(lifecycleListObj.size() > 0) {
           				Iterator iter = lifecycleListObj.keySet().iterator();
           				while(iter.hasNext()) {
           					boolean current = true;
           					String key = (String)iter.next();
           					lifecycleObj = (JSONObject)lifecycleListObj.get(key);
           					String s=null, startdate="", enddate="";
           					long lifecycleId = 0;
           					if(lifecycleObj.get("id") instanceof Long) {
           						lifecycleId = ((Long)lifecycleObj.get("id")).longValue();
           					} else if(lifecycleObj.get("id") instanceof Integer) {
           						lifecycleId = (long)((Integer)lifecycleObj.get("id")).intValue();
           					} else {
           						try {
           							lifecycleId = Long.parseLong((String)lifecycleObj.get("id"));
           						} catch(Exception e) {
           							lifecycleId = System.currentTimeMillis();
           						}
           					}
           					if(lifecycleObj.has("current")) {
           						if(lifecycleObj.get("current") instanceof Boolean) {
           							s = ((Boolean)lifecycleObj.get("current")).toString();
           						} else {
           							s = (String)lifecycleObj.get("current");
           						}
           						if(s.equals(Double.toString(lifecycleId))) {
           							current = true;
           						} else if(s.equals("true")) {
           							current = true;
           						} else if(s.equals("false")) {
           							current = false;
           						} else if(s.equals("Y")) {
           							current = true;
           						} else if(s.equals("N")) {
           							current = false;
           						}
           					} else {
           						current = true;
           					}
           					if(lifecycleObj.has("startdate")) {
           						startdate = (String)lifecycleObj.get("startdate");
           						if(startdate.indexOf("\\") != -1) {
           							startdate = startdate.replaceAll("\\","");
           						}
           					}
           					if(lifecycleObj.has("enddate")) {
           						enddate = (String)lifecycleObj.get("enddate");
           						if(enddate.indexOf("\\") != -1) {
           							enddate = enddate.replaceAll("\\","");
           						}
           					}
           					lifecycleObj.put("id", lifecycleId);
           					lifecycleObj.put("current", current);
           					lifecycleObj.put("startdate", startdate);
           					lifecycleObj.put("enddate", enddate);
           					
           					arr = new JSONArray();
           					arr.add(lifecycleObj);
           				}
           			} else {
           				arr = new JSONArray();
           			}
           			System.out.println("Processed JSON Object");
           		}
           	} else {
           		arr = new JSONArray();
           	}
           	
           	System.out.println(arr.toString());
           	PreparedStatement pst1 = conn.prepareStatement(updateSQL);
           	pst1.setString(1, arr.toString());
           	pst1.setString(2, id);
           	pst1.executeUpdate();
           	pst1.close();
        }
        rs.close();
        pst.close();
        conn.commit();
        conn.close();
    	
    }
    
    private void resetCapabilityFactor() throws Exception {
    	String sql = "select id, business_capability_factor, business_capability_asset_factor from enterprise_business_capability";
    	String updateSQL = "update enterprise_business_capability set business_capability_factor=?, business_capability_asset_factor=? where id = ?";
    	PreparedStatement pst = conn.prepareStatement(sql);
    	String risk="LOW", threatRisk="0", assetRisk="0";
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
	        String id = rs.getString("id");
            String factor = rs.getString("business_capability_factor");
            String assetFactor = rs.getString("business_capability_asset_factor");
            
            OrderedJSONObject factorListObj = null;
            if(factor != null && factor.length() > 0) {
	            factorListObj = new OrderedJSONObject(factor);
	            if(factorListObj.has("risk")) {
		        	factorListObj.remove("risk");
		    	}
	        } else {
	        	factorListObj = new OrderedJSONObject();
	        }
	        
		    Iterator iter = factorListObj.keySet().iterator();
		    while(iter.hasNext()) {
		    	String key = (String) iter.next();
		    	OrderedJSONObject factorObj = (OrderedJSONObject)factorListObj.get(key);
		    	factorObj.put("factor", "0");
		    	factorObj.put("risk", risk);
		    	factorListObj.put(key, factorObj);
		    }
	        
	        OrderedJSONObject assetfactorListObj = null;
            if(assetfactorListObj != null && assetfactorListObj.length() > 0) {
	            assetfactorListObj = new OrderedJSONObject(assetFactor);
	            if(assetfactorListObj.has("risk")) {
		        	assetfactorListObj.remove("risk");
		    	}
	        } else {
	        	assetfactorListObj = new OrderedJSONObject();
	        }
	        
		    iter = assetfactorListObj.keySet().iterator();
		    while(iter.hasNext()) {
		    	String key = (String) iter.next();
		    	OrderedJSONObject assetfactorObj = (OrderedJSONObject)assetfactorListObj.get(key);
		    	assetfactorObj.put("asset risk", threatRisk);
	        	assetfactorObj.put("threat risk", assetRisk);
	        	assetfactorListObj.put(key, assetfactorObj);
		    }
	        
	        PreparedStatement pst1 = conn.prepareStatement(updateSQL);
           	pst1.setString(1, factorListObj.toString());
           	pst1.setString(2, assetfactorListObj.toString());
           	pst1.setString(3, id);
           	pst1.executeUpdate();
           	pst1.close();
          
        }
        rs.close();
        pst.close();
        conn.commit();
        conn.close();
	}
    
    private void deleteCapability(String str) throws Exception {
    	String sql = "select id,child_capability_name from enterprise_business_capability where parent_capability_name = ?";
    	String deleteSQL = "delete from enterprise_business_capability where parent_capability_id = ?";
		String deleteChildSQL = "delete from enterprise_business_capability where id = ?";
		String deleteRootSQL = "delete from enterprise_business_capability where child_capability_name = ?";
    	
    	PreparedStatement pst = conn.prepareStatement(sql);
    	pst.setString(1, str);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
	        String id = rs.getString("id");
			String name = rs.getString("child_capability_name");
			System.out.println(name);
	        PreparedStatement pst1 = conn.prepareStatement(deleteSQL);
	        pst1.setString(1, id);
	        pst1.executeUpdate();
	        pst1.close();
	        pst1 = conn.prepareStatement(deleteChildSQL);
	        pst1.setString(1, id);
	        pst1.executeUpdate();
	        pst1.close();
	        deleteChildCapability(name);
	    }
	    rs.close();
		pst.close();
		
		pst = conn.prepareStatement(deleteRootSQL);
		pst.setString(1, str);
		pst.executeUpdate();

		pst.close();
	    conn.commit();
	    conn.close();
    }
    
    private void deleteChildCapability(String str) throws Exception {
    	System.out.println("Deleting:"+str);
    	String sql = "select id,child_capability_name from enterprise_business_capability where parent_capability_name = ?";
    	String deleteSQL = "delete from enterprise_business_capability where parent_capability_id = ?";
    	
    	PreparedStatement pst = conn.prepareStatement(sql);
    	pst.setString(1, str);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
	        String id = rs.getString("id");
	        String name = rs.getString("child_capability_name");
	        PreparedStatement pst1 = conn.prepareStatement(deleteSQL);
	        pst1.setString(1, id);
	        pst1.executeUpdate();
	        pst1.close();
	        deleteChildCapability(name);
	    }
	    rs.close();
	    pst.close();
	    
	    conn.commit();
    }
    
    private void copyCapability(String fromstr, String tostr, String fromCompanyCode) throws Exception {
    	String parentSQL = "select id from enterprise_business_capability where child_capability_name = ?";
    	String sql = "select id, child_capability_name, business_capability_factor, business_capability_asset_factor, business_capability_plan_factor from "+fromCompanyCode+".enterprise_business_capability where parent_capability_name = ?";
		String insertSQL = "insert into enterprise_business_capability(id, parent_capability_id,child_capability_name,parent_capability_name,business_capability_factor,business_capability_asset_factor, business_capability_plan_factor,created_by,updated_by,company_code) value(?,?,?,?,?,?,?,'support@smarterd.com','support@smarterd.com',?)";
		String deleteSQL = "delete from enterprise_business_capability where id = ?";
		String parentId = null, fromId=null;
		
		// Get Id of the from capability
    	PreparedStatement pst = conn.prepareStatement(parentSQL);
    	pst.setString(1, fromstr);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
	        fromId = rs.getString("id");
	    }
	    rs.close();
	    pst.close();
    	
    	// Get Id where the tree needs to be inserted
    	pst = conn.prepareStatement(parentSQL);
    	pst.setString(1, tostr);
        rs = pst.executeQuery();
        while (rs.next()) {
	        parentId = rs.getString("id");
	    }
	    rs.close();
	    pst.close();
	    System.out.println("Root=>"+tostr+":"+parentId);
	    pst = conn.prepareStatement(sql);
    	pst.setString(1, fromstr);
        rs = pst.executeQuery();
        while (rs.next()) {
	        String id = rs.getString("id");
	        String name = rs.getString("child_capability_name");
	        String factor = rs.getString("business_capability_factor");
	        String assetfactor = rs.getString("business_capability_asset_factor");
	        String planfactor = rs.getString("business_capability_plan_factor");
	        if(name.equals(fromstr)) {
	        	continue;
			}
			if(db.equals(fromCompanyCode)) {
				PreparedStatement pst1 = conn.prepareStatement(deleteSQL);
				pst1.setString(1, id);
				pst1.executeUpdate();
				pst1.close();
				conn.commit();
			}
	        System.out.println(name+":"+id);
	        PreparedStatement pst1 = conn.prepareStatement(insertSQL);
	        pst1.setString(1, id);
	        pst1.setString(2, parentId);
	        pst1.setString(3, name);
	        pst1.setString(4, tostr);
	        pst1.setString(5, factor);
	        pst1.setString(6, assetfactor);
	        pst1.setString(7, planfactor);
	        pst1.setString(8, db);
	        pst1.executeUpdate();
	        pst1.close();
	        copyChildCapability(id, name, fromCompanyCode);
	        
	    }
	    rs.close();
		pst.close();

		if(db.equals(fromCompanyCode)) {
			PreparedStatement pst1 = conn.prepareStatement(deleteSQL);
			pst1.setString(1, fromId);
			pst1.executeUpdate();
			pst1.close();
			conn.commit();
		}

		conn.commit();
    }
    
    private void copyChildCapability(String parentId, String parentName, String fromCompanyCode) throws Exception {
    	String sql = "select id, child_capability_name, business_capability_factor, business_capability_asset_factor, business_capability_plan_factor from "+fromCompanyCode+".enterprise_business_capability where parent_capability_id = ?";
    	String insertSQL = "insert into enterprise_business_capability(id, parent_capability_id,child_capability_name,parent_capability_name,business_capability_factor,business_capability_asset_factor, business_capability_plan_factor,created_by,updated_by,company_code) value(?,?,?,?,?,?,?,'support@smarterd.com','support@smarterd.com',?)";
		String deleteSQL = "delete from enterprise_business_capability where id = ?";
		
    	// Get Id where the tree needs to be inserted
    	PreparedStatement pst = conn.prepareStatement(sql);
    	pst.setString(1, parentId);
        ResultSet rs = pst.executeQuery();
        while (rs.next()) {
	        String id = rs.getString("id");
	        String name = rs.getString("child_capability_name");
	        String factor = rs.getString("business_capability_factor");
	        String assetfactor = rs.getString("business_capability_asset_factor");
			String planfactor = rs.getString("business_capability_plan_factor");
			if(db.equals(fromCompanyCode)) {
				PreparedStatement pst1 = conn.prepareStatement(deleteSQL);
				pst1.setString(1, id);
				pst1.executeUpdate();
				pst1.close();
				conn.commit();
			}

	        System.out.println(name+":"+id);
	        PreparedStatement pst1 = conn.prepareStatement(insertSQL);
	        pst1.setString(1, id);
	        pst1.setString(2, parentId);
	        pst1.setString(3, name);
	        pst1.setString(4, parentName);
	        pst1.setString(5, factor);
	        pst1.setString(6, assetfactor);
	        pst1.setString(7, planfactor);
	        pst1.setString(8, db);
	        pst1.executeUpdate();
	        pst1.close();
	        copyChildCapability(id, name, fromCompanyCode);
	        
	    }
	    rs.close();
		pst.close();
		conn.commit();
    }

	public static void main(String[] args) throws Exception {
		String host = args[0];
		String db = args[1];
		String command = args[2];
		
		LoadAttributeMetadata loader = new LoadAttributeMetadata(host, db);
		if(command.equals("metadata")) {
			ArrayList list = loader.getTablesMetadata();
			loader.getColumnsMetadata(list);
		} else if(command.equals("lifecycle_format_update")) {
			loader.updateLifecycleFormat();
		} else if(command.equals("reset_capability_factor")) {
			loader.resetCapabilityFactor();
		} else if(command.equals("delete_capability")) {
			String value1 = args[3];
			System.out.println(value1);
			loader.deleteCapability(value1);
		} else if(command.equals("copy_capability")) {
			String value1 = args[3];
			String value2 = args[4];
			String value3 = args[5];
			System.out.println(value1+":"+value2+":"+value3);
			loader.copyCapability(value1, value2, value3);
		} 
		
	}
}

		