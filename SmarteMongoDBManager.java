package com.smarterd.util;

import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.BasicDBObjectBuilder;
import static com.mongodb.client.model.Filters.eq;
import com.mongodb.util.JSON;
import org.bson.types.ObjectId;
import org.bson.Document;
import org.apache.wink.json4j.OrderedJSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.net.URLEncoder;
import java.util.Arrays;

public class SmarteMongoDBManager {
    private static final Logger logger = LoggerFactory.getLogger(SmarteMongoDBManager.class);
    private static String hostname = "localhost";
    private static int port = 27017;
    private static String username = "smartEuser";
    private static String pwd = "smarterD2018!";
    private MongoClient mongoClient = null;
    private MongoDatabase db = null;
    private int count = 0;

    public SmarteMongoDBManager() {
    }

    // Initialize Connection and Bulk operation
    public void connect(String dbname) throws Exception {
        //MongoCredential credential = MongoCredential.createCredential(username, dbname, pwd.toCharArray());
        //mongoClient = new MongoClient(Arrays.asList(new ServerAddress(hostname, port)), Arrays.asList(credential));
        String encoded_pwd = URLEncoder.encode(pwd, "UTF-8");
        //System.out.println(encoded_pwd);
        MongoClientURI connectionString = new MongoClientURI("mongodb://"+username+":"+encoded_pwd+"@localhost:27017/"+dbname);
        mongoClient = new MongoClient(connectionString);
        db = mongoClient.getDatabase(dbname);
        System.out.println("Successfully connected to Mongo DB:"+dbname);
    }

    public void close()  throws Exception {
        mongoClient.close();
    }

    public String getDocument(String coll, String id) throws Exception {
    	System.out.println("In getDocument:"+coll+":"+id+"...");
        MongoCollection<Document> collection = db.getCollection(coll);
        Document doc = collection.find(eq("_id", new ObjectId(id))).first();
        return doc.toJson();
    }

    public MongoDatabase getDatabase(String inputDbName) {
        return getDatabase(inputDbName, false);
    }

    public MongoDatabase getDatabase(String inputDbName, boolean create) {
        MongoIterable<String> dbNames = mongoClient.listDatabaseNames();
        for (String dbName : dbNames) {
            if (dbName.equals(inputDbName)) {
                return mongoClient.getDatabase(inputDbName);
            }
        }
        if (create) {
            return mongoClient.getDatabase(inputDbName);
        }
        return null;
    }


    // Insert Document into Bulk object
    public void insertDocument(String collectionName, String doc) throws Exception {
        MongoCollection<Document> coll= db.getCollection(collectionName);
        DBObject dbObject = (DBObject) JSON.parse(doc);
        //bulkWriteOperation = coll.initializeUnorderedBulkOperation();
        //bulkWriteOperation.insert(dbObject);
        count++;
    }

    // Insert documents into collection
    public void UpdateDB() throws Exception {
        logger.info("Successfully inserted documents!!!");
    }

    // Delete all documents from Collection
    public void remove(String collectionName, DBObject dbObject) throws Exception {
    }

    public void initClient() {
        MongoCredential credential = MongoCredential.createCredential(username, "admin", pwd.toCharArray());
        mongoClient = new MongoClient(Arrays.asList(new ServerAddress(hostname, port)), Arrays.asList(credential));
    }

    public static void main(String[] args) throws Exception {
        SmarteMongoDBManager mgr = new SmarteMongoDBManager();
        mgr.connect("DEMO");
        String str = mgr.getDocument("ThreatAsset","5bad1b6e3901343a5358bdf4");
        System.out.println(str);
        OrderedJSONObject doc = new OrderedJSONObject(str);
        OrderedJSONObject extObj = (OrderedJSONObject)doc.get("extension");
        System.out.println(extObj.get("riskComp"));
        /*
        doc.put("name", "Vijay");
        doc.put("address", "1041");
        mgr.insertDocument("A700001", doc.toString());
        */
        mgr.close();
    }
}
